<?php
use Cake\Routing\Router;

Router::plugin('ContactsManager', ['path' => '/interno/contatos'], function ($routes) {
	$routes->connect('/', ['controller' => 'Contacts', 'action' => 'index']);
	$routes->connect('/ler/:id', ['controller' => 'Contacts', 'action' => 'read'], ['pass' => ['id']]);
});
