<?php
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;

$configDir = dirname(__FILE__) . DS;

try {
  if(file_exists(CONFIG . '/contacts_manager.php')) {
    Configure::load('contacts_manager', 'default', false);
  }
  else {
    Configure::config('contacts_manager_config', new PhpConfig($configDir));
    Configure::load('default_settings', 'contacts_manager_config', false);
    Configure::drop('contacts_manager_config');
  }
}
catch(\Exception $e) {
  die($e->getMessage());
}
