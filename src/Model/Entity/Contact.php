<?php
namespace ContactsManager\Model\Entity;

use ContactsManager\Model\Entity\Entity;

/**
* Contact Entity.
*/
class Contact extends Entity
{

  protected $_accessible = [
    '*' => true,
    'id' => false
  ];

  public function getDate($format = 'dd/MM/YYYY HH:mm:ss') {
    return $this->created->i18nFormat($format);
  }
}
