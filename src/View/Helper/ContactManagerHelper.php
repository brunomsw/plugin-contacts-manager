<?php
namespace ContactsManager\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * ContactManager helper
 */
class ContactManagerHelper extends Helper
{
	public function writeLastContacts($contacts)
	{
		return $this->element('ContactManager.last_contacts', ['contacts' => $contacts]);
	}
}
