<?php
namespace ContactsManager\Controller;

use ContactsManager\Controller\AppController;

class ContactsController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadModel('ContactsManager.Contacts');
  }

  public function index()
  {
    $contacts = $this->Contacts->getAllContacts();
    $this->set('contacts', $contacts);
  }

  public function read($id)
  {
    $contact = $this->Contacts->get($id);
    $this->set('contact', $contact);
  }
}
