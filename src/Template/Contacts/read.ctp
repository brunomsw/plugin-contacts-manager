<div class="row">
	<div class="col-md-6">
		<h1 class="page-header">Contato: <?= $contact->name ?></h1>
		<h3>Telefone: <?= $contact->phone ?></h3>
		<h4>Email: <?= $contact->email ?></h4>
		<p><strong>Data:</strong> <?= $contact->getDate() ?></p>
		<h3>Mensagem</h3>
		<p><?= $contact->message ?></p>
	</div>
</div>
