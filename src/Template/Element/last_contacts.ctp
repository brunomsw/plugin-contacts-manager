<h1 class="page-header">Últimos contatos feitos pelo site</h1>
<?php
	echo $this->Html->tag('table', null, ['class' => 'table stripped table-bordered realties-table']);
	
	echo $this->Html->tag('thead', $this->Html->tableHeaders(['Nome', 'Telefone', 'Email', 'Data', 'Mensagem', 'Opções']));
	$cells = [];

	foreach($contacts as $contact){
		$options = [];
		$options[] = $this->Html->link('Ler mais', '/interno/contatos/ler/' . $contact->id, ['class' => 'btn btn-sm btn-primary']);
		$cells[] = [$contact->name, $contact->phone, $contact->email, $contact->getData(), \Cake\Utility\Text::truncate($contact->message, 100), implode(' ', $options)];
	}
	echo $this->Html->tableCells($cells);

	echo $this->Html->tag('/table');

?>
<p><em>Para acessar a lista completa, <?= $this->Html->link('Clique aqui', '/interno/contatos') ?></em></p>